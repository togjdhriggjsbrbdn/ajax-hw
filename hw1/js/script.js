const dm = new DomManager();
const film = new Film();

function getCharacters(url) {
    return fetch();
}



film
    .getFilms()
    .then(films => {
        films.forEach(item => {

            const div = dm.createElement('div');
            const filmItem = new FilmItem(item);

            dm.setHtml(div, film.buildCard(item));
            dm.addClass(div, 'film-item');
            dm.appendToBody(div);

            const character = new Character();

            Promise.all(
                filmItem
                    .characters
                    .map(url => {
                        return character.getCharacters(url);
                    })



            )
                .then(charResponse => {
                    const actors = charResponse.map(ac => new CharacterItem(ac));

                    const div2 = dm.createElement('div');

                    dm.setHtml(
                        div2,
                        character.buildHtml(actors)
                    );

                    dm.appendToElement(div, div2);

                });
            // character.getCharacters()

            console.log(film, filmItem.characters);



        });
    })
    // .finally(() => {
    //
    //     // document.querySelectorAll('.btn-show-more-film')
    //
    // });
