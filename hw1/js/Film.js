class Film extends Api{
    constructor() {
        super();
    }

    getFilms() {
        return super
            .get('https://swapi.co/api/films/')
            .then(r => {
            if (!Array.isArray(r.results)) {
                return [];
            }

            return r.results;
        });
    }

    buildCard(film) {
        return `<h1>${film.title}</h1><button class="btn-show-more-film" id="btn${(Math.random()*100).toFixed()}">Show more</button>`;
    }
}