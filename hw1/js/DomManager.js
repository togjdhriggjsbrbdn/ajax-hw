class DomManager {
    constructor() {
        this._element = null;
    }

    createElement(element) {
        this._element = document.createElement(element);

        return this._element;
    }

    setHtml(element, html) {
        element
            .innerHTML = html;
    }

    addClass(element, cssClass) {
        element
            .classList
            .add(cssClass);
    }

    appendToElement(element, node) {
        element.appendChild(node);
    }

    appendToBody(element) {
        document.body.appendChild(element);
    }
}