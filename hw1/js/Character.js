class Character extends Api {
    constructor() {
        super();
    }

    /**
     *
     **/
    getCharacters(url) {
        return super
            .get(url);
    }

    buildHtml(actors) {
        return '<ul>' + actors.map(a => `<li>${a.name}</li>`)
            .join('') + '</ul>';
    }
}